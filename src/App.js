import { DragDropContextProvider } from 'react-dnd'
import './App.css'
import Board from './Board'
import HTML5Backend from 'react-dnd-html5-backend'

function App() {
  return (
    <div>
      <DragDropContextProvider backend={HTML5Backend}>
        <Board />
      </DragDropContextProvider>
    </div>
  )
}

export default App
