import React from 'react'
import Square from './Square'
import Knight from './Knight'

import {
  canMoveKnight,
  moveKnight,
  registerObserver,
  unRegisterObserver,
} from './Game'
import { useState, useEffect } from 'react'
import DropZone from './DropZone'

function renderSquare(i, [knightX, knightY]) {
  const x = i % 8
  const y = Math.floor(i / 8)
  const isKnightHere = knightX === x && knightY === y
  const isBlack = (x + y) % 2 === 1
  const piece = isKnightHere ? <Knight /> : null

  return (
    <DropZone
      key={i}
      onDrop={() => moveKnight(x, y)}
      canDrop={() => canMoveKnight(x, y)}
    >
      <Square isBlack={isBlack}>{piece}</Square>
    </DropZone>
  )
}

export default function Board() {
  var [knightPosition, setKnightPosition] = useState([0, 0])

  useEffect(() => {
    let observer = (newPosition) => setKnightPosition(newPosition)
    registerObserver(observer)
    return () => unRegisterObserver(observer)
  }, [])

  const squares = []
  for (let i = 0; i < 64; i++) {
    squares.push(renderSquare(i, knightPosition))
  }

  return (
    <div
      style={{
        width: '80vw',
        height: '80vw',
        display: 'flex',
        flexWrap: 'wrap',
      }}
    >
      {squares}
    </div>
  )
}
