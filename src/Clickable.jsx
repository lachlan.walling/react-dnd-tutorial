export let withClick = (clickHandler, Child) => (childProps) => {
  return (
    <div onClick={clickHandler}>
      <Child {...childProps}/>
    </div>
  )
}

/*
  const ClickableSquare = withClick(clickHandler, Square)
*/