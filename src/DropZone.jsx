import { ItemTypes } from "./Constants"
import { DropTarget } from "react-dnd"

const dropZoneTarget = {
  drop(props) {
    props.onDrop()
  },

  canDrop(props) {
    return props.canDrop()
  }
}

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop()
  }
}

function renderOverlay(colour) {
return (
  <div
  style={{
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    zIndex: 1,
    opacity: 0.5,
    backgroundColor: colour,
  }}
></div>
)
}

function DropZone({ connectDropTarget, isOver, canDrop, children }) {
  return connectDropTarget(
    <div
      style={{
        position: 'relative',
      }}
    >
      {children}
      {isOver && canDrop && renderOverlay('green')}
      {!isOver && canDrop && renderOverlay('yellow')}
      {isOver && !canDrop && renderOverlay('red')}
    </div>,
  )
}


export default DropTarget(ItemTypes.KNIGHT, dropZoneTarget, collect)(DropZone)