let knightPosition = [0, 0]
let observer = null

function emitChange() {
  if (observer !== null) {
    observer(knightPosition)
  }
}

export function registerObserver(newObserver) {
  if (observer) {
    throw new Error('Multiple observers not implemented.')
  }
  observer = newObserver
  emitChange()
}

export function unRegisterObserver(oldObserver) {
  if (observer !== oldObserver) {
    throw new Error(`Observer isn't registered`)
  }
  observer = null
}

export function canMoveKnight(toX, toY) {
  const [x, y] = knightPosition
  const dx = Math.abs(toX - x)
  const dy = Math.abs(toY - y)
  return Math.min(dx, dy) === 1 && Math.max(dx, dy) === 2
}

export function moveKnight(toX, toY) {
  knightPosition = [toX, toY]
  emitChange()
}
