import React from 'react'

export default function Square({ isBlack, children }) {
  const fill = isBlack ? 'black' : 'white'
  const stroke = isBlack ? 'white' : 'black'

  return (
    <div
      style={{
        backgroundColor: fill,
        color: stroke,
        width: '10vw',
        height: '10vw',
        boxSizing: 'content-box',
      }}
    >
      {children}
    </div>
  )
}
